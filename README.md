![logo TripMaster.png](./logo TripMaster.png)
***
# **TourGuide** 
***
TourGuide est une application Spring Boot déjà développée, à destination des touristes.
 Cette application permet aux utilisateurs de voir quels sont les attractions touristiques proches de leur position actuelle et d’obtenir des réductions sur les séjours d’hôtel ainsi que sur les billets de différents spectacles.
 Grâce à notre réseau d'agences de voyages, nous proposons des séjours et activités qui correspondent à tous les critères des clients. (Nombres total de voyageurs, Prix, Durée du voyage, etc.)
 Dans l’application on pourra obtenir des points de récompenses pour visiter les attractions touristiques, échangeable contre des réductions sur les voyages.
***
***
## Built With
***
- Spring Boot
- Java
- Gradle
- JUnit
- JaCoCo
***
***
## Pour commencer
Cloner le repository:
https://gitlab.com/codedidier/tourguide_da_java_oc_p8-V3-0.git
***
***
## Architechture Diagram:
![Schéma de conception technique](./image.png)
***
***
## EndPoints TourGuide
![EndPoints.png](./image-1.png)
***
