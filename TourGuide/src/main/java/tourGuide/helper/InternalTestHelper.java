package tourGuide.helper;

/**
 * InternalTestHelper est utilisé pour la création d'utilisateurs fictifs à des fins de test.
 */
public class InternalTestHelper {

	// Set this default up to 100,000 for testing
	private static int internalUserNumber = 10000;

/**
 * Méthode pour mettre à jour le nombre d'utilisateurs à créer
 *
 * @param internalUserNumber nombre d'utilisateurs à créer
 */
	public static void setInternalUserNumber(int internalUserNumber) {
		InternalTestHelper.internalUserNumber = internalUserNumber;
	}

	/**
	 * Méthode de récupération du nombre d'utilisateurs à créer
	 *
	 * @return la valeur actuelle de internalUserNumber
	 */
	public static int getInternalUserNumber() {
		return internalUserNumber;
	}
}
