package tourGuide.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.user.User;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.List;
@Service
public class TripService {
    private Logger logger = LoggerFactory.getLogger(TripService.class);
    private final TripPricer tripPricer;

    private static final String tripPricerApiKey = "test-server-api-key";


    public TripService(TripPricer tripPricer) {
        this.tripPricer = tripPricer;
    }

    /**
     * Obtenir des offres de voyage pour un utilisateur
     * Demande à TripPricer des informations sur l'utilisateur (y compris les préférences telles que le nombre d'adultes/enfants).
     * TripPricer répond avec 5 offres de voyage pour l'utilisateur en fonction des exigences fournies.
     *
     * @param user utilisateur pour lequel des offres doivent être trouvées
     * Retourne les offres suggérées sous la forme d'une liste d'objets Provider.
     */
    public List<Provider> getTripDeals(User user) {
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(i -> i.getRewardPoints()).sum();
        List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

}
