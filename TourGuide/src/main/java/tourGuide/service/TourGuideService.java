package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.dto.NearByAttraction;
import tourGuide.dto.UserLocation;
import tourGuide.helper.InternalTestHelper;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

/**
 * TourGuideService effectue des opérations pour TourGuideController
 */
@Service
public class TourGuideService {
	private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
	private final GpsService gpsService;
	private final RewardsService rewardsService;
	private final TripService tripService;
	private final UserService userService;
	public final Tracker tracker;
	boolean testMode = true;
	private final ConcurrentHashMap<String, User> internalUserMap = new ConcurrentHashMap<>();
	private int threadPoolSize = 500;
	private ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);

	public TourGuideService(GpsService gpsService, RewardsService rewardsService, UserService userService, TripService tripService) {
		this.gpsService = gpsService;
		this.rewardsService = rewardsService;
		this.userService = userService;
		this.tripService = tripService;

		if (testMode) {
			logger.info("TestMode enabled");
			logger.debug("Initializing users");
			initializeInternalUsers();
			logger.debug("Finished initializing users");
		}
		tracker = new Tracker(this);
		addShutDownHook();
	}
	/**
	 * Obtenir les récompenses pour l'utilisateur.
	 *
	 * @param userName User name
	 * @return liste d'objets UserReward pour l'utilisateur.
	 */
	public List<UserReward> getUserRewards(String userName) {
		User user = getUserByUsername(userName);
		if (user == null) {
			logger.debug("getUserRewards: user not found with name " + userName + " returning null");
			return null;
		}

		return user.getUserRewards();
	}
	/**
	 * Obtenir l'emplacement actuel de l'utilisateur
	 *
	 * @param userName User name
	 * @return localisation actuelle de l'utilisateur
	 */
	public VisitedLocation getUserLocation(String userName) {
		User user = getUserByUsername(userName);
		if (user==null) {
			logger.debug("getUserLocation: user not found with name " + userName + " returning null");
			return null;
		}

		VisitedLocation visitedLocation = (user.getVisitedLocations().size() > 0) ?
				user.getLastVisitedLocation() :
				trackUserLocation(user);
		return visitedLocation;
	}
	/**
	 * Obtention de l'objet User pour le nom d'utilisateur
	 *
	 * @param userName User name
	 * @return user object
	 */
	public User getUserByUsername(String userName) {
		return userService.getUserByUsername(userName);
	}

	public User getUser(String userName) {
		return internalUserMap.get(userName);
	}
	/**
	 * Obtenir les objets User pour tous les utilisateurs actuellement dans le système.
	 *
	 * @return une liste d'objets utilisateur pour tous les utilisateurs.
	 */
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	/**
	 * Obtenir le nombre d'utilisateurs actuellement stockés dans le système
	 *
	 * @retourne le nombre d'utilisateurs actuellement stockés, sous forme d'int.
	 */
	public int getUserCount() {
		return userService.getUserCount();
	}
	/**
	 * Ajouter un utilisateur au système
	 *
	 * @param user user
	 */
	public void addUser(User user) {
	userService.addUser(user);
	}
	/**
	 * Obtenir des offres de voyage pour un utilisateur à partir du TripService.
	 *
	 * @param userName User name
	 * @return Liste d'objets Provider pour les offres de voyage suggérées par TripService
	 */
	public List<Provider> getTripDeals(String userName) {
		User user = getUserByUsername(userName);
		if (user == null) {
			logger.debug("getTripDeals: user not found with name " + userName + " returning null");
			return null;
		}
		return tripService.getTripDeals(getUserByUsername(userName));
	}

	/**
	 * Track user's current location
	 * Obtient l'emplacement de l'utilisateur à partir du GpsService.
	 * Puis ajoute cet emplacement aux emplacements visités par l'utilisateur.
	 * Ensuite demande au service de récompense de calculer les récompenses de l'utilisateur pour les mettre à jour avec la nouvelle localisation.
	 *
	 * @param user Utilisateur à localiser
	 * Retourne la VisitedLocation de l'emplacement actuel de l'utilisateur.
	 */
	public VisitedLocation trackUserLocation(User user) {
		VisitedLocation visitedLocation = gpsService.getUserLocation(user.getUserId());
		CompletableFuture.supplyAsync(() -> userService.addToVisitedLocations(visitedLocation, user.getUserName()), executorService)
				.thenAccept(n -> {
					rewardsService.calculateRewards(user);
				});

		return visitedLocation;
	}

	/**
	 * Track user's current location
	 * Obtient l'emplacement de l'utilisateur à partir du GpsService.
	 * Puis ajoute cet emplacement aux emplacements visités par l'utilisateur.
	 * Ensuite demande au service de récompense de calculer les récompenses de l'utilisateur pour les mettre à jour avec la nouvelle localisation.
	 *
	 * @param userName Nom de l'utilisateur à suivre.
	 * Retourne VisitedLocation de l'emplacement actuel de l'utilisateur.
	 */
	public VisitedLocation trackUserLocationByUsername(String userName) {

		User user = getUserByUsername(userName);
		if (user == null) {
			logger.debug("trackUserLocationByUsername: user not found with name " + userName + " returning null");
			return null;
		}

		VisitedLocation visitedLocation = gpsService.getUserLocation(user.getUserId());

		CompletableFuture.supplyAsync(()-> {
					return userService.addToVisitedLocations(visitedLocation, user.getUserName());
				}, executorService)
				.thenAccept(n -> {rewardsService.calculateRewards(user);});

		return visitedLocation;
	}
	/**
	 * Track all users' current location
	 * Pour chaque utilisateur :
	 * Obtient l'emplacement de l'utilisateur à partir du GpsService.
	 * Ajoute ensuite cet emplacement aux emplacements visités par l'utilisateur.
	 *
	 */
	public void trackAllUserLocations() {
		List<User> allUsers = userService.getAllUsers();

		ArrayList<CompletableFuture> futures = new ArrayList<>();
		logger.debug("trackAllUserLocations: Creating futures for " + allUsers.size() + " user(s)");

		allUsers.forEach((n) -> {
			futures.add(
					CompletableFuture.supplyAsync(()-> {
						return userService.addToVisitedLocations(gpsService.getUserLocation(n.getUserId()), n.getUserName());
					}, executorService)
			);
		});
		logger.debug("trackAllUserLocations: Futures created: " + futures.size() + ". Getting futures...");
		futures.forEach((n)-> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Track All Users InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Track All Users ExecutionException: " + e);
			}
		});
		logger.debug("trackAllUserLocations: Done!");


	}
	/**
	 * Track all users' current location, puis vérifie si de nouvelles récompenses sont déclenchées.
	 * Pour chaque utilisateur :
	 * Obtient la position de l'utilisateur à partir du GpsService.
	 * Ajoute ensuite cet emplacement aux emplacements visités par l'utilisateur.
	 * Ensuite demande au service de récompenses de calculer les récompenses de l'utilisateur et de les mettre à jour en fonction de la nouvelle position.
	 *
	 */
	public void trackAllUserLocationsAndProcess() {


		List<User> allUsers = userService.getAllUsers();

		ArrayList<CompletableFuture> futures = new ArrayList<>();

		logger.debug("trackAllUserLocationsAndProcess: Creating futures for " + allUsers.size() + " user(s)");
		allUsers.forEach((n) -> {
			futures.add(
					CompletableFuture.supplyAsync(() -> {
								return userService.addToVisitedLocations(gpsService.getUserLocation(n.getUserId()), n.getUserName());
							}, executorService)
							.thenAccept(y -> {
								rewardsService.calculateRewards(n);
							})
			);
		});
		logger.debug("trackAllUserLocationsAndProcess: Futures created: " + futures.size() + ". Getting futures...");
		futures.forEach((n) -> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Track All Users And Process InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Track All Users And Process ExecutionException: " + e);
			}
		});
		logger.debug("Done!");

	}

	/**
	 * Traite toutes les récompenses en attente pour tous les utilisateurs
	 * Pour chaque utilisateur :
	 * Demande au service de récompense de calculer les récompenses de l'utilisateur et de les mettre à jour avec les nouveaux lieux visités.
	 *
	 */
	public void processAllUserRewards() {


		List<User> allUsers = userService.getAllUsers();

		ArrayList<CompletableFuture> futures = new ArrayList<>();

		logger.debug("processAllUserRewards: Creating threads for " + allUsers.size() + " user(s)");
		allUsers.forEach((n)-> {
			futures.add(
					CompletableFuture.supplyAsync(()-> {
						return rewardsService.calculateRewardsReturn(n);
					}, executorService)
			);
		});
		logger.debug("processAllUserRewards: Futures created: " + futures.size() + ". Getting futures...");
		futures.forEach((n)-> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Process All User Rewards InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Process All User Rewards ExecutionException: " + e);
			}
		});
		logger.debug("processAllUserRewards:Done!");

	}

	/**
	 * Obtenir les emplacements actuels de tous les utilisateurs à partir de l'UserService.
	 *
	 * @return La liste de tous les emplacements actuels des utilisateurs sous forme d'objets UserLocation
	 */
	public List<UserLocation> getAllCurrentLocations() {
		return userService.getAllCurrentLocations();
	}

	/**
	 * Obtenir les cinq attractions les plus proches de l'emplacement
	 * Récupère toutes les attractions du GpsService, et les trie en fonction de la distance par rapport à l'emplacement fourni.
	 * Puis renvoie les cinq plus proches
	 *
	 * @param visitedLocation emplacement à utiliser
	 * @return Liste de cinq objets NearbyAttraction
	 */
	public List<NearByAttraction> getNearByAttractions(VisitedLocation visitedLocation) {
		List<Attraction> attractionsList;
		Map<Double, Attraction> attractionsMap = new HashMap<>();

		//Cré une carte de distance/localisation, et la met dans TreeMap pour la trier par distance.
		gpsService.getAttractions().forEach((n) -> {
			attractionsMap.put(getDistance(n, visitedLocation.location), n);
		});
		TreeMap<Double, Attraction> sortedAttractionMap = new TreeMap<>(attractionsMap);

		//Cré une liste des 5 attractions les plus proches.
		if (sortedAttractionMap.size() >= 5) {
			attractionsList = new ArrayList<>(sortedAttractionMap.values()).subList(0, 5);
		} else {
			logger.debug("getNearByAttractions: gpsService provided less than 5 attractions");
			attractionsList = new ArrayList<>(sortedAttractionMap.values()).subList(0, sortedAttractionMap.size());
		}

		//Cré une liste d'entités de sortie contenant uniquement les données souhaitées
		List<NearByAttraction> output = new ArrayList<>();
		attractionsList.forEach((n) -> {
			output.add(new NearByAttraction(n.attractionName,
					n.latitude, n.longitude, visitedLocation.location.latitude, visitedLocation.location.longitude,
					getDistance(n, visitedLocation.location), rewardsService.getRewardValue(n, visitedLocation.userId)));

		});

		return output;
	}

	private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
	private int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;

	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	public double getDistance(Location loc1, Location loc2) {
		double lat1 = Math.toRadians(loc1.latitude);
		double lon1 = Math.toRadians(loc1.longitude);
		double lat2 = Math.toRadians(loc2.latitude);
		double lon2 = Math.toRadians(loc2.longitude);

		double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
				+ Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

		double nauticalMiles = 60 * Math.toDegrees(angle);
		double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
		return statuteMiles;
	}

	private void addShutDownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				executorService.shutdown();tracker.stopTracking();
			}
		});
	}

	public UserPreferences updateUserPreferences(String userName, UserPreferences userPreferences) {
		User user = getUserByUsername(userName);
		if (user == null) {
			logger.debug("updateUserPreferences: user not found with name " + userName + " returning null");
			return null;
		}
		user.getUserPreferences().setTripDuration(userPreferences.getTripDuration());
		user.getUserPreferences().setNumberOfChildren(userPreferences.getNumberOfAdults());
		user.getUserPreferences().setNumberOfAdults(userPreferences.getNumberOfChildren());
		return user.saveUser(userPreferences);
	}


	/**********************************************************************************
	 * Methods Below: For Internal Testing
	 *
	 **********************************************************************************/
	//private static final String tripPricerApiKey = "test-server-api-key";
	// Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory

	private void initializeInternalUsers() {
		IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
			String userName = "internalUser" + i;
			String phone = "000";
			String email = userName + "@tourGuide.com";
			User user = new User(UUID.randomUUID(), userName, phone, email);
			generateUserLocationHistory(user);

			userService.addUser(user);
		});
		logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
	}

	private void generateUserLocationHistory(User user) {
		IntStream.range(0, 3).forEach(i -> {
			user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
		});
	}

	private double generateRandomLongitude() {
		double leftLimit = -180;
		double rightLimit = 180;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private double generateRandomLatitude() {
		double leftLimit = -85.05112878;
		double rightLimit = 85.05112878;
		return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
	}

	private Date getRandomTime() {
		LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
		return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
	}

}