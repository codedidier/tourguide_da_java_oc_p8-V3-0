package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.dto.UserLocation;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * UserService contient la collection d'utilisateurs et exécute les tâches associées à l'application TourGuide principale.
 * Pour permettre un accès simultané, nous utilisons un ConcurrentMap pour stocker les utilisateurs.
 * Cette classe serait à adapter pour qu'elle fonctionne avec une base de données ou une solution de stockage similaire.
 */
@Service
public class UserService {
    private Logger logger = LoggerFactory.getLogger(UserService.class);
    private ConcurrentMap<String, User> usersByName;

    public UserService() {
        final int CAPACITY = 100;
        usersByName = new ConcurrentHashMap<String, User>(CAPACITY);
    }
    /**
     * Ajouter un utilisateur
     *
     * @param user Objet utilisateur à ajouter
     * @return boolean true si réussi, false si l'utilisateur existe déjà avec ce userName
     */
    public boolean addUser(User user){
        if(!usersByName.containsKey(user.getUserName())) {
            usersByName.put(user.getUserName(), user);
            return true;
        }
        return false;
    }
    /**
     * Ajout d'une VisitedLocation à un utilisateur.
     *
     * @param visitedLocation objet VisitedLocation à ajouter à l'utilisateur
     * @param userName Nom d'utilisateur
     * @Return un booléen true si réussi, false si l'utilisateur n'a pas été trouvé.
     */
    public boolean addToVisitedLocations(VisitedLocation visitedLocation, String userName) {
        User user = usersByName.get(userName);
        if (user != null) {
            user.addToVisitedLocations(visitedLocation);
            return true;
        }
        logger.debug("addToVisitedLocations: user not found with name " + userName + " returning false");
        return false;
    }
    /**
     * Obtenir les emplacements actuels de tous les utilisateurs
     *
     * @return Liste d'objets UserLocation
     */
    public List<UserLocation> getAllCurrentLocations() {
        List<UserLocation> userLocations = new ArrayList<>();
        usersByName.forEach((k,v)-> {
            userLocations.add(new UserLocation(v.getUserId(), v.getLastVisitedLocation()));
        });
        return userLocations;
    }
    /**
     * Génère et ajoute une UserReward à un utilisateur.
     *
     * @param userName Nom d'utilisateur
     * @param visitedLocation visitedLocation
     * @param attraction l'attraction pour laquelle la récompense est attribuée
     * @param rewardPoints les points à ajouter
     * Retourne le booléen true si réussi, false si l'utilisateur n'a pas été trouvé.
     */
    public boolean addUserReward(String userName, VisitedLocation visitedLocation, Attraction attraction, int rewardPoints) {
        User user = getUserByUsername(userName);
        if (user != null) {
            user.addUserReward(new UserReward(visitedLocation, attraction, rewardPoints));
            return true;
        }
        logger.debug("addUserReward: user not found with name " + userName + " returning false");
        return false;

    }
    /**
     * Obtenir tous les utilisateurs
     *
     * @return Liste des utilisateurs
     */
    public List<User> getAllUsers() {
        return usersByName.values().stream().collect(Collectors.toList());
    }

    /**
     * Récupère un utilisateur userName
     * Retourne null si l'utilisateur n'existe pas
     *
     * @param userName Nom d'utilisateur
     * @return objet User
     */
    public User getUserByUsername(String userName) {
        return usersByName.get(userName);
    }

    /**
     * Obtenir le nombre de tous les utilisateurs actuellement stockés dans le système.
     *
     @return Le nombre d'utilisateurs actuellement stockés
     */
    public int getUserCount() {
        return usersByName.size();
    }

}
