package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

@Service
public class RewardsService {
	private Logger logger = LoggerFactory.getLogger(RewardsService.class);
	private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles
	private int defaultProximityBuffer = 100;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	private final GpsService gpsService;
	private final RewardCentral rewardsCentral;
	private final UserService userService;

	private int threadPoolSize = 500;
	private ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);

	public RewardsService(GpsService gpsService, RewardCentral rewardCentral, UserService userService) {
		this.gpsService = gpsService;
		this.rewardsCentral = rewardCentral;
		this.userService = userService;

	}
	/**
	 * Set une valeur pour Proximity Buffer
	 * Permet le contrôle de la proximité de l'emplacement d'un utilisateur
	 * par rapport à une attraction pour obtenir des récompenses.
	 *
	 * @param proximityBuffer int
	 */
	public void setProximityBuffer(int proximityBuffer) {
		logger.debug("setProximityBuffer: updating proximity buffer to " + proximityBuffer);
		this.proximityBuffer = proximityBuffer;
	}
	/**
	 * Remet Proximity Buffer à sa valeur par défaut,
	 * telle que stockée dans defaultProximityBuffer.
	 */
	public void setDefaultProximityBuffer() {
		logger.debug("setDefaultProximityBuffer: resetting proximity buffer to " + defaultProximityBuffer);
		proximityBuffer = defaultProximityBuffer;
	}
	/**
	 * Vérifie si un emplacement fourni est près d'une attraction.
	 *
	 * @param attraction attraction à vérifier
	 * @param location location à comparer
	 *
	 * @Return true si l'emplacement est à portée, sinon false.
	 */
	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > attractionProximityRange ? false : true;
	}

	/**
	 * Vérifier si une VisitedLocation est à portée d'une Attraction
	 * @return true or false
	 */
	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	/**
	 * Obtenir les RewardPoints d'une attraction pour un UUID donné.
	 */
	private int getRewardPoints(Attraction attraction, UUID userId) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, userId);
	}
	/**
	 * Obtenir la valeur en points de récompense d'une attraction pour un utilisateur.
	 *
	 * @param userid UUID de l'utilisateur
	 * @param attraction attraction à vérifier
	 * @return en int la valeur de la récompense
	 */
	public int getRewardValue(Attraction attraction, UUID userid) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, userid);
	}

	/**
	 * Donne la distance entre deux Locations
	 * @param loc1 Location 1
	 * @param loc2 Location 2
	 */
	public double getDistance(Location loc1, Location loc2) {
		double lat1 = Math.toRadians(loc1.latitude);
		double lon1 = Math.toRadians(loc1.longitude);
		double lat2 = Math.toRadians(loc2.latitude);
		double lon2 = Math.toRadians(loc2.longitude);

		double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
				+ Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

		double nauticalMiles = 60 * Math.toDegrees(angle);
		double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
		return statuteMiles;
	}
	/**
	 * Calcule les récompenses pour un utilisateur fourni
	 * Vérifie toutes les VisitedLocations de l'utilisateur, les compare à la liste des Attractions du GpsService.
	 * Si un utilisateur a visité une attraction (c'est-à-dire que l'emplacement visité est à proximité d'une attraction).
	 * Ajoute une récompense à l'utilisateur pour cette attraction s'il n'en a pas déjà reçu une.
	 *
	 * @param user Objet User
	 */
	public void calculateRewards(User user) {
		List<VisitedLocation> userLocations = user.getVisitedLocations();
		List<Attraction> attractions = gpsService.getAttractions();

		ArrayList<CompletableFuture> futures = new ArrayList<>();

		for (VisitedLocation visitedLocation : userLocations) {
			for (Attraction attr : attractions) {
				futures.add(
						CompletableFuture.runAsync(() -> {
							if (user.getUserRewards().stream().filter(r -> r.attraction.attractionName.equals(attr.attractionName)).count() == 0) {

								if (nearAttraction(visitedLocation, attr)) {
									userService.addUserReward(user.getUserName(), visitedLocation, attr, getRewardPoints(attr, user.getUserId()));
								}
							}
						}, executorService)
				);
			}
		}

		futures.forEach((n) -> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Calculate Rewards InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Calculate Rewards ExecutionException: " + e);
			}
		});


	}

	/**
	 * Calcule les récompenses pour un utilisateur
	 * Vérifie toutes les VisitedLocations de l'utilisateur, les compare à la liste des Attractions du GpsService.
	 * Si un utilisateur a visité une attraction (c'est-à-dire que l'emplacement visité est à proximité d'une attraction).
	 * Ajoute une récompense à l'utilisateur pour cette attraction s'il n'en a pas déjà reçu une.
	 *
	 * @param user Objet User
	 * @return userName
	 */
	public String calculateRewardsReturn(User user) {

		List<VisitedLocation> userLocations = user.getVisitedLocations();
		List<Attraction> attractions = gpsService.getAttractions();

		CopyOnWriteArrayList<CompletableFuture> futures = new CopyOnWriteArrayList<CompletableFuture>();

		for(VisitedLocation visitedLocation : userLocations) {
			for (Attraction attr : attractions) {
				futures.add(
						CompletableFuture.runAsync(()-> {
							if(user.getUserRewards().stream().filter(r -> r.attraction.attractionName.equals(attr.attractionName)).count() == 0) {

								if(nearAttraction(visitedLocation, attr)) {
									userService.addUserReward(user.getUserName(), visitedLocation, attr, getRewardPoints(attr, user.getUserId()));
								}
							}
						},executorService)
				);
			}
		}

		futures.forEach((n)-> {
			try {
				n.get();
			} catch (InterruptedException e) {
				logger.error("Calculate Rewards InterruptedException: " + e);
			} catch (ExecutionException e) {
				logger.error("Calculate Rewards ExecutionException: " + e);
			}
		});

		return user.getUserName();
	}

}