package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class GpsService {
    private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    private final GpsUtil gpsUtil;

    public GpsService(GpsUtil gpsUtil) {
        this.gpsUtil = gpsUtil;
    }

    /**
     * Get User Location
     * Interroge GpsUtil pour obtenir la localisation de l'utilisateur avec UUID fourni.
     *
     * @param userId UUID de l'utilisateur à localiser
     * @return la localisation actuelle de l'utilisateur
     */
    public VisitedLocation getUserLocation(UUID userId) {
        return gpsUtil.getUserLocation(userId);
    }

/**
 * Get Attractions
 * Interroge GpsUtil pour obtenir toutes les attractions disponibles.
  * Return une liste d'objets Attraction pour toutes les attractions disponibles.
 */
 public List<Attraction> getAttractions() {
        return new ArrayList<Attraction>(gpsUtil.getAttractions());
    }

}
