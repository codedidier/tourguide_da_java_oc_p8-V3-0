package tourGuide.dto;

/**
 * NearbyAttraction est une entité utilisée pour permettre le formatage correct
 * des réponses au point de terminaison /getNearbyAttractions.
 *
 */
public class NearByAttraction {
    String attractionName;
    Double attractionLatitute;
    Double attractionLongitude;
    Double userLatitute;
    Double userLongitude;
    Double distance;
    int rewardPoints;

    public NearByAttraction(String attractionName, Double attractionLatitute, Double attractionLongitude, Double userLatitute, Double userLongitude, Double distance, int rewardPoints) {
        this.attractionName = attractionName;
        this.attractionLatitute = attractionLatitute;
        this.attractionLongitude = attractionLongitude;
        this.userLatitute = userLatitute;
        this.userLongitude = userLongitude;
        this.distance = distance;
        this.rewardPoints = rewardPoints;
    }

}
