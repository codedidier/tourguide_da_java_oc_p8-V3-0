package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;

import java.util.List;

/**
 * Endpoints Tourguide
 */

@RestController
public class TourGuideController {
    private Logger logger = LoggerFactory.getLogger(TourGuideController.class);

	@Autowired
	TourGuideService tourGuideService;

    /**
     *  Mapping pour l'URL racine
     *  Ne prend pas de paramètres, renvoie simplement un message de bienvenue.
     * @return "Greetings from TourGuide!"
     */
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }
    /**
     * Mapping for Get
     * Retourne :
     * la localisation de l'utilisateur si celui-ci existe, au format : {"longitude":xxx, "latitude":yyy}
     * "User Not Found [userName]" si l'utilisateur n'est pas dans le système.
     *
     * @param userName nom de l'utilisateur
     * @return une chaîne Json de l'emplacement de l'utilisateur.
     */
    @RequestMapping("/getLocation") 
    public String getLocation(@RequestParam String userName) {
        logger.info("getLocation: endpoint called.");
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(userName);
        if (visitedLocation == null) {
            logger.info("getLocation: requested user not found.");
            return JsonStream.serialize("User Not Found [" + userName + "]");
        }
        logger.info("getLocation: returning user location.");

        return JsonStream.serialize(visitedLocation.location);
    }
    
    //Fait: Change this method to no longer return a List of Attractions.
 	//  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
 	//  Return a new JSON object that contains:
    	// Name of Tourist attraction, 
        // Tourist attractions lat/long, 
        // The user's location lat/long, 
        // The distance in miles between the user's location and each of the attractions.
        // The reward points for visiting each Attraction.
        //    Note: Attraction reward points can be gathered from RewardsCentral

    /**
     * Mapping for Get
     * Retourne :
     * Les cinq attractions touristiques les plus proches pour l'utilisateur, quelle que soit la distance.
     * La réponse comprend :
     * -Nom de l'attraction
     * -Latitude/longitude de l'attraction
     * Latitude et longitude de l'utilisateur
     * Distance en miles entre l'emplacement de l'utilisateur et l'attraction.
     * Les points de récompense pour cette combinaison attraction/utilisateur.
     * Retourne "User Not Found [userName]" si l'utilisateur n'est pas dans le système.
     *
     * @param userName nom de l'utilisateur
     * @return Json avec cinq attractions les plus proches de l'utilisateur.
     */
    @RequestMapping("/getNearbyAttractions") 
    public String getNearByAttractions(@RequestParam String userName) {
        logger.info("getNearbyAttractions: endpoint called.");
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(userName);
        if (visitedLocation == null) {
            logger.info("getNearbyAttractions: requested user not found.");
            return JsonStream.serialize("User Not Found [" + userName + "]");
        }
        logger.info("getNearbyAttractions: returning nearByAttractions.");
        return JsonStream.serialize(tourGuideService.getNearByAttractions(visitedLocation));
    }

    /**
     * Mapping for Get
     * Retourne :
     * Objet Json des récompenses actuelles de l'utilisateur,
     * sous la forme d'une liste d'objets UserReward.
     * "User Not Found [userName]" si l'utilisateur n'est pas dans le système.
     *
     * @param userName nom de l'utilisateur
     */
    @RequestMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        logger.info("getRewards: endpoint called.");
        List<UserReward> userRewards = tourGuideService.getUserRewards(userName);
        if (userRewards == null) {
            logger.info("getRewards: requested user not found.");
            return JsonStream.serialize("User Not Found [" + userName + "]");
        }
        logger.info("getRewards: returning user rewards.");

        return JsonStream.serialize(userRewards);
    }

    /**
     * Mapping for Get
     * Retourne :
     * Objet Json de toutes les localisations actuelles des utilisateurs, au format :
     * {"userId":idString, "longitude":xxx, "latitude":yyy},{"userId":idString, "longitude":xxx, "latitude":yyy}].
     * Si aucun utilisateur n'est stocké, une liste vide est retournée.
     *
     * @return Json de la localisation actuelle de tous les utilisateurs.
     */
    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
        logger.info("getAllCurrentLocations: endpoint called. Returning all current locations");
    	// FAIT: Get a list of every user's most recent location as JSON
    	//- Note: does not use gpsUtil to query for their current location, 
    	//        but rather gathers the user's current location from their stored location history.
    	//
    	// Return object should be the just a JSON mapping of userId to Locations similar to:
    	//     {
    	//        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371} 
    	//        ...
    	//     }
    	
    	return JsonStream.serialize(tourGuideService.getAllCurrentLocations());
    }

    /**
     * Mapping for Get
     * Retourne :
     * Objet Json contenant 5 offres de voyage pour l'utilisateur.
     * Comprend le nom de l'attraction, le prix et le tripId.
     * "User Not Found [userName]" si l'utilisateur n'est pas dans le système.
     *
     * @param userName Nom d'utilisateur de l'utilisateur
     */
    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        logger.info("getTripDeals: endpoint called.");
        List<Provider> providers = tourGuideService.getTripDeals(userName);
        if (providers == null) {
            logger.info("getTripDeals: requested user not found.");
            return JsonStream.serialize("User Not Found [" + userName + "]");
        }
        logger.info("getTripDeals: returning trip deals for user.");
        return JsonStream.serialize(providers);
    }
    private User getUser(@RequestParam String userName) {
        return tourGuideService.getUser(userName);
    }

    /**
     * Mapping for Put
     * Mise à jour des preferences utilisateur
     * "User Not Found [userName]" si l'utilisateur n'est pas dans le système.
     *
     * @param userName        Nom d'utilisateur
     * @param userPreferences Preferences utilisateur
     * @return Objet Json contenant les UserPreferences
     */
 @PutMapping("/updateUserPreferences")
 public UserPreferences updateUserPreferences(@RequestParam String userName, @RequestBody UserPreferences userPreferences) {
     UserPreferences userUpdate = tourGuideService.updateUserPreferences(userName, userPreferences);
    if (userUpdate == null) {
        logger.info("updateUser: PUT User Not Found");
        return null;
    }
         logger.info("updateUser: PUT successful request");
         return userUpdate;

 }}